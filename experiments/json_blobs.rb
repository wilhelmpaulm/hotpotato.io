require 'Mysql2'
require 'benchmark'

def func1
  (0..10_000).each do |x|
    puts x
  end
end

# def func2
#   (0..2500).each do |x|
#     puts x
#   end
# end
#
# def func3
#   (0..2500).each do |x|
#     puts x
#   end
# end
#
# def func4
#   (0..2500).each do |x|
#     puts x
#   end
# end

puts Benchmark.measure {
  func1
  # t1 = Thread.new { func1 }
  # t2 = Thread.new { func2 }
  # t3 = Thread.new { func3 }
  # t4 = Thread.new { func4 }
  # t1.join
  # t2.join
  # t3.join
  # t4.join
}
